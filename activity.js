/*
- What directive is used by Node.js in loading the modules it needs?
	Answer: Require
- What Node.js module contains a method for server creation?
	Answer: Built-in
- What is the method of the http object responsible for creating a server using Node.js?
	Answer: createServer
- What method of the response object allows us to set status codes and content types?
	Answer: writeHead
- Where will console.log() output its contents when run in Node.js?
	Answer: Server
- What property of the request object contains the address's endpoint?
	Answer: End
*/

const http = require('http');


const port = 3000


const server = http.createServer((request,response) => {

	if(request.url == '/login'){
		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end('You are in the login page')
	}
	// Access the '/homepage' route and will return a message of "This is the hompage"
	else if(request.url == '/homepage'){
		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end('This is the homepage')
	}

	// All other routes will return a message of "Page not available"
	// Set status code for the response - 404

	else {
	response.writeHead(404,{'Content-Type':'text/plain'})
	response.end('Page not available')
	}

})

server.listen(port)

console.log(`Server now accessible at localhost:${port}`)